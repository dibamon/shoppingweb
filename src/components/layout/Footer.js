import React from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link'

function Footer (){
    return(
        <footer >
            <Box bgcolor="text.secondary" color="white" px={{xs:3 ,sm:10}} py={{xs:5,sm:9}} mt={4}>
                <Container>
                    <Grid container spacing={5}>
                        <Grid item xs={12} sm={4}>
                            <Box borderBottom={1}>خدمات مشتریان</Box>
                            <Box>
                                <Link href="/" color="inherit" >پاسخ به پرسش های متداول</Link>
                            </Box>
                            <Box>
                                <Link href="/" color="inherit" >حریم خصوصی</Link>
                            </Box>
                            <Box>
                                <Link href="/" color="inherit" >گزارش باگ</Link>
                            </Box>
                        </Grid>

                        <Grid item xs={12} sm={4}>
                            <Box borderBottom={1}>راهنمای خرید</Box>
                            <Box>
                                <Link href="/" color="inherit" >نحوه ثبت سفارش</Link>
                            </Box>
                            <Box>
                                <Link href="/" color="inherit" >رویه ارسال سفارش</Link>
                            </Box>
                            <Box>
                                <Link href="/" color="inherit" >شیوه های پرداخت</Link>
                            </Box>
                        </Grid>

                        <Grid item xs={12} sm={4}>
                            <Box borderBottom={1}>با ما</Box>
                            <Box>
                                <Link href="/" color="inherit" >اتاق خبر</Link>
                            </Box>
                            <Box>
                                <Link href="/" color="inherit" >تماس با ما</Link>
                            </Box>
                            <Box>
                                <Link href="/" color="inherit" >درباره ما</Link>
                            </Box>
                        </Grid>
                    </Grid>
                     <Box textAlign="center" pt={{ xs: 5, sm: 10 }} pb={{ xs: 5, sm: 0 }}>
                        designed by diba &reg;{new Date().getFullYear()}
                     </Box>
                </Container>
            </Box>
        </footer>
    )
}

export default Footer ; 