import React from "react";
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Divider, Grid, IconButton } from "@material-ui/core";
import CardMedia from '@material-ui/core/CardMedia';
import Paper from '@material-ui/core/Paper';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { DeleteOutlined } from "@material-ui/icons";
import NumericInput from 'react-numeric-input';
import '../../asset/css/shoppingCarts.css';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';


const useStyles = makeStyles((theme)=>({
    root: {
        height: 250,
        maxWidth: '100%',
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
        padding : 16 ,
    },
    mediaContent: {
        display: 'flex',
        paddingTop: 17 ,
        height: 137,

    }, 
    
    cover: {
        width: 160,
        height: 125,
        
    }, 
    
    content: {
        flex: '1 0 auto',
    },

    controls: {
        display: 'flex',
        alignItems: 'center',
        paddingRight: theme.spacing(23),
        paddingBottom: theme.spacing(1),
        height: 60
    },
    img: {
        maxWidth: '100%',
        maxHeight: '100%',
    },
   

}));

const Carts = ({ item, quantity, deleteBt})=>{
    const classes = useStyles();

    const deleteHandlet = ()=>{
        deleteBt(item.id)
    };

    return(
        <Card className={classes.root}  >
            <div className={classes.details} >
                <div className={classes.mediaContent}>
                    <div className={classes.cover}>
                        <img alt={item.alt} className={classes.img} src={item.image[0]} />
                    </div>
                        {/* <CardMedia
                            className={classes.cover}
                            image="https://dkstatics-public.digikala.com/digikala-products/4308872.jpg?x-oss-process=image/resize,w_1280/quality,q_80"
                            title="Live from space album cover"
                        /> */}
                    
                    <CardContent className={classes.content}>
                        <Typography component="h5" variant="h5">
                            {item.title}
                        </Typography>
                        <Typography variant="subtitle1" color="textSecondary">
                            فروشنده: پارسیان
                        </Typography>
                    </CardContent>
                </div>
                
                <div className={classes.controls}>
                    <NumericInput min={0} max={3} mobile size={2} className="form-control" value={quantity} style={{
                        wrap: {
                            borderRadius: '6px 3px 3px 6px',
                            height : 35
                        },
                        input: {
                            borderRadius: '4px 2px 2px 4px',
                            border: '1px solid #ccc',
                            height: 35,
                            color: '#039be5',
                            },
                        }} />

                    <Box style={{marginRight:2}}>
                    <IconButton onClick={deleteHandlet}>
                        <DeleteOutlined/>
                        <Typography variant="subtitle1" color="textSecondary">
                            حذف
                        </Typography>
                    </IconButton>
                    </Box>

                    <Box flexGrow={1} >
                        <IconButton>
                          <BookmarkBorderIcon/>
                            <Typography variant="subtitle1" color="textSecondary">
                                نشان کردن
                            </Typography>
                        </IconButton>
                    </Box>

                    <Box style={{marginLeft:16}}>
                        <Typography>تومان {item.prise}</Typography>
                    </Box>
                   
                </div>
            </div>
        </Card>
    )
}


export default Carts ;
















{/* <Grid container spacing={2}>
    <Grid item>
        <CardMedia
            className={classes.cover}
            image="/static/images/cards/live-from-space.jpg"
            title="Live from space album cover"
        />
    </Grid>
    <Grid item xs container direction="column" spacing={2} >
        <Grid item xs >
           
        </Grid>
        <Grid item >
           
        </Grid>
    </Grid>
</Grid> */}


{/* <Grid container spacing={2} style={{ width: '100%', height: '100%' }}>
    <Grid item container direction="column" spacing={2} sm={4} xs={4}  >

        <Grid item sm xs >
            <Paper style={{ backgroundColor: "skyblue", width: '100%', height: '100%' }} />
        </Grid>
        <Grid item sm xs >
            <Paper style={{ backgroundColor: "skyblue", width: '100%', height: '100%' }} />
        </Grid>

    </Grid>
    <Grid item container direction="column" spacing={2} sm={8} xs={8} >
        <Paper style={{ backgroundColor: 'red', width: 50, height: 350 }} />
    </Grid>
</Grid> */}