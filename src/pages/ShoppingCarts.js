import React, { useEffect, useState } from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardHeader from '@material-ui/core/CardHeader';
import Typography from '@material-ui/core/Typography';
import { Divider } from '@material-ui/core';
import Carts from '../components/shoppingCarts/Carts';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 250,
        width: '100%',
    },
    control: {
        padding: theme.spacing(2),
    },
}));


const ShoppingCarts = () => {
    const classes = useStyles();
    const [data, setData] = useState([]);
    let counter=[];
    let edit = data[0] || [];
    

    useEffect(() => {
        if (localStorage.getItem("product") === null) {
            localStorage.setItem("product", JSON.stringify([]));
        } else {
            let list = JSON.parse(localStorage.getItem("product"));
            setData([list]);
         }
    }, [])


    function count() {
        for (let i = 0; i < edit.length ; i++){
            let number = 1; //ya 0 bezar
            for (let j = 0; j < edit.length; j++) {
                if(i!==j){
                if(edit[i].id === edit[j].id){
                    number+=1;
                    edit = edit.filter((item, index) => index !==j );
                    j-=1
                }
                }
            }
            
            counter[i]= number;
        }
        
    };
    
    count();
    


    const deleteBt = (id)=>{
        const result = data[0].filter(re=> re.id !== id);

        localStorage.setItem("product", JSON.stringify(result));
        setData([result]);
    }
    


    return (
        <div>
            <Box bgcolor="grey.200" color="white" px={{ xs: 2, sm: 7 }} py={{ xs: 5, sm: 5 }} mt={1}>
                <Container>
                    <Grid container spacing={4}>
                        <Grid item xs={12} md={8} container direction="column" spacing={2}>
                            {edit.map((el,index) => (
                                <Grid item ke={el.id}>
                                    <Carts item={el} quantity={counter[index]} deleteBt={deleteBt} />
                                </Grid>
                            ))}
                        </Grid>

                        <Grid item xs={12} md={4} >
                            <Box>
                                <Card >
                                    <CardHeader
                                        title="مجموع کل سبد خرید"
                                    />
                                    <CardContent>
                                        <Box display="flex" py={1} >
                                            <Box flexGrow={1}><Typography>قیمت کالاها</Typography></Box>
                                            <Box><Typography>۵۸۸,۴۰۰ تومان</Typography></Box>
                                        </Box>

                                        <Box display="flex" pb={2} pt={1} >
                                            <Box flexGrow={1}><Typography>تخفیف کالاها</Typography></Box>
                                            <Box><Typography>۱۱۷,۶۰۰ تومان</Typography></Box>
                                        </Box>
                                        <Divider />
                                        <Box display="flex" py={2} >
                                            <Box flexGrow={1}><Typography>جمع سبد خرید</Typography></Box>
                                            <Box><Typography>۴۷۰,۸۰۰ تومان</Typography></Box>
                                        </Box>
                                    </CardContent>

                                    <CardActions>
                                        <Button variant="contained" color="secondary" disableElevation fullWidth size="large" >
                                            اقدام به پرداخت
                                        </Button>
                                    </CardActions>
                                </Card>
                            </Box>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </div>
    );
};


export default ShoppingCarts;
















